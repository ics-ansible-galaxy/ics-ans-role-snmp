ics-ans-role-snmp
===================

Ansible role to install snmp.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

Role Variables
--------------

```yaml
snmp_community:
  - "public"
snmp_location: 
  - "[55.710407,13.2151365]"
  - "MV server room"
snmp_contact: "CSControlSystemInfrastructuregroup@esss.se"
snmp_port: 161
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-snmp
```

License
-------

BSD 2-clause
