import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


def test_config_file_exists(host):
    file = host.file("/etc/snmp/snmpd.conf")
    assert file.exists
    assert file.mode == 0o600
    assert file.user == "root"


def test_snmpd_enabled_and_running(host):
    service = host.service("snmpd")
    assert service.is_running
    assert service.is_enabled


def test_snmp_walk(host):
    snmp = host.run("snmpwalk -Os -v 2c -c public localhost iso.3.6.1.2.1.1.1.0")
    assert "Linux ics-ans-role-snmp" in snmp.stdout
